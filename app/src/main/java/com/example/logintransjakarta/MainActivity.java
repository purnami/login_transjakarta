package com.example.logintransjakarta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button login;
    Transjakarta tj;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login=findViewById(R.id.login);
        tj=new Transjakarta(getApplicationContext());
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickLogin("admin".getBytes(),"admin".getBytes());
            }
        });
    }

    private void clickLogin(byte[] username, byte[] password) {
        if(username==null) {
            return;
        }
        if(password==null)
        {
            return;
        }

        byte[] result=tj.pcd_cmd_login(username,password);
        tj.STISendData(result);
    }
}