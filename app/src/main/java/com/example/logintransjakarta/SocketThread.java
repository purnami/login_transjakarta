package com.example.logintransjakarta;

import android.util.Log;

import com.example.logintransjakarta.helper.ByteUtils;
import com.example.logintransjakarta.helper.Hex;
import com.example.logintransjakarta.helper.converter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import static android.content.ContentValues.TAG;
import static com.example.logintransjakarta.helper.ErrorCode.ERR_CRC;
import static com.example.logintransjakarta.helper.ErrorCode.OK;

public class SocketThread
{
    private byte[] data;
    String ip;
    int port;
    String TAG_log = "Socket";

    Socket socket=null;
    private Socket mSocket;
    private OutputStream mOutputStream;
    private InputStream mInputStream;

    OutputStream os=null;
    byte[] tempbuffer=null;
    byte[] buffer=new byte[1000];
    int offset=0;
    int avail;
    private boolean isStop = false;

    public void connect(String ip, int port) {
        this.ip = ip;
        this.port = port;

        try {
            socket = new Socket(ip, port);
            socket.setSoTimeout(5000);
            Log.d("connected", "success");
        }catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
            return;
        }
    }

    public byte[] sendData(byte[] data){
        this.data=data;
        try {
            os = socket.getOutputStream();
        }catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
            return data;
        }
        try {
            os.write(data);

        }catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
            return data;
        }
        try {
            Thread.sleep(1000);
        }
        catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
        }
        try {
            avail=socket.getInputStream().available();
                while(avail>0) {
                    tempbuffer = new byte[avail];
                    socket.getInputStream().read(tempbuffer);

                    System.arraycopy(tempbuffer, 0, buffer, offset, tempbuffer.length);
                    offset += tempbuffer.length;
                    avail = socket.getInputStream().available();
                }
            Log.d(TAG,"resp : "+ Hex.bytesToHexString(buffer,0,offset));
        }
        catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
        }

        tempbuffer=new byte[offset];
        System.arraycopy(buffer,0,tempbuffer,0,tempbuffer.length);
        Log.d("tempbuffer", ""+Hex.bytesToHexString(tempbuffer));
        return tempbuffer;
    }

    public void close(){
        try{
            os.flush();
            socket.shutdownOutput();
            os.close();
            socket.close();
        }catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
        }
    }

}
