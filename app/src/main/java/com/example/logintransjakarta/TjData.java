package com.example.logintransjakarta;

public class TjData {
    public static String armadaid;
    public static String plat;
    public static String no_body;
    public static String trip_id;
    public static String si_id;
    public static String gate_in;
    public static String deduct_gate_in;
    public static String shelter_code_gate_in;
    public static String terminal_code_gate_in;
    public static String interop;
    public static String gate_out_on;
    public static String terminal_code_gate_out;
    public static String gate_in_on;
    public static String shelter_code_gate_out;
    public static String jaklingko_subsidy_accumulation;
    public static String journey_counter;
    public static String expiration_on;
    public static String mid;
    public static String tid;
    public static String status;
    public static String trans_code;
    public static String desc;
    public static String gratis;
    public static String transportation_type;
    public static String jaklingko_first_gate_in;
    protected static byte[] username=new byte[20];
    protected static byte[] password=new byte[100];
    protected static byte[] response=new byte[1024];
    protected static String ip;
    protected static int port;
    protected static String token;
    protected static String direction_id;
    protected static String direction_name;
    protected static Boolean loginSuccess=false;
    protected static String bcdtimestamp;
    protected static String normal_fare;
    protected static String balance_after;
    protected static String fare;
    protected static String balance_before;
    protected static String min_balance;
    protected static String lat;
    protected static String lng;
    protected static byte[] card_no=new byte[100];
    protected static String card_type;

}
