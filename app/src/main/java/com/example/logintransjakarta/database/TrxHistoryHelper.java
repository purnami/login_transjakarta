package com.example.logintransjakarta.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import static com.example.logintransjakarta.database.DatabaseContract.TABLE_TRXHISTORY;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.AMOUNT;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.BALANCE;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.BANKLOG;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.CARDNO;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.CARDTYPE;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.EXP;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.INTEROP;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.LAT;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.LNG;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.PARTNERDATA;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.SYNC;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.SYNCTIME;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.TAPTYPE;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.TIMESTAMP;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.UID;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns._ID;

public class TrxHistoryHelper {
    private TrxHistoryHelper(Context context) {
        dataBaseHelper = new DatabaseHelper(context);
    }

    private static final String DATABASE_TABLE = TABLE_TRXHISTORY;
    private static DatabaseHelper dataBaseHelper;
    private static TrxHistoryHelper INSTANCE;

    private static SQLiteDatabase database;

    public static TrxHistoryHelper getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (SQLiteOpenHelper.class) {
                if (INSTANCE == null) {
                    INSTANCE = new TrxHistoryHelper(context);
                }
            }
        }
        return INSTANCE;
    }

    public void open() throws SQLException {
        database = dataBaseHelper.getWritableDatabase();
    }

    public void close() {
        dataBaseHelper.close();

        if (database.isOpen())
            database.close();
    }

    public Cursor queryAll() {
        return database.query(DATABASE_TABLE,
                null,
                null,
                null,
                null,
                null,
                _ID + " DESC");
    }
//
    public Cursor queryById(String id) {
        return database.query(DATABASE_TABLE, null
                , _ID + " = ?"
                , new String[]{id}
                , null
                , null
                , null
                , null);
    }
    public Cursor queryByColumn(String colName) {
        return database.query(DATABASE_TABLE, new String[]{colName}
                , null
                , null
                , null
                , null
                , null
                , null);
    }

//    public ArrayList<String> getDataByColumn(String colName){
//        ArrayList<String> arrayList = new ArrayList();
//        Cursor c = database.query(DATABASE_TABLE, new String[]{colName}, null, null, null, null, null, null);
//        if (c.moveToFirst()) {
//            do {
//                String col = c.getString(c.getColumnIndex(colName));
//                arrayList.add(col);
//            } while(c.moveToNext());
//        }
//        return arrayList;
//    }

    public ArrayList<String> getDataByColumn(String colName, String query){
        ArrayList<String> arrayList = new ArrayList();
        String selectQuery = "SELECT "+colName+" FROM "+TABLE_TRXHISTORY+" " + query;
        SQLiteDatabase db = this.dataBaseHelper.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, (String[])null);
        if (c.moveToFirst()) {
            do {
                String col = c.getString(c.getColumnIndex(colName));
                arrayList.add(col);
            } while(c.moveToNext());
        }
        return arrayList;
    }

    public long insert(ContentValues values) {
        return database.insert(DATABASE_TABLE, null, values);
    }

    public int update(String id, ContentValues values) {
        return database.update(DATABASE_TABLE, values, _ID + " = ?", new String[]{id});
    }

    public int deleteById(String id) {
        return database.delete(DATABASE_TABLE, _ID + " = " + id, null);
    }

    public ArrayList<TrxHistory> getAllData(String query){
        ArrayList<TrxHistory> trxArrayList = new ArrayList();
        String selectQuery = "SELECT * FROM "+TABLE_TRXHISTORY+" "+query;
//        String selectQuery = query;
        SQLiteDatabase db = this.dataBaseHelper.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, (String[])null);
        if (c.moveToFirst()) {
            do {
                int id = c.getInt(c.getColumnIndex(_ID));
                String uid = c.getString(c.getColumnIndex(UID));
                String cardtype= c.getString(c.getColumnIndex(CARDTYPE));
                String interop= c.getString(c.getColumnIndex(INTEROP));
                String cardno= c.getString(c.getColumnIndex(CARDNO));
                String amount= c.getString(c.getColumnIndex(AMOUNT));
                String balance= c.getString(c.getColumnIndex(BALANCE));
                String exp= c.getString(c.getColumnIndex(EXP));
                String timestamp= c.getString(c.getColumnIndex(TIMESTAMP));
                String banklog= c.getString(c.getColumnIndex(BANKLOG));
                String partnerdata= c.getString(c.getColumnIndex(PARTNERDATA));
                String taptype= c.getString(c.getColumnIndex(TAPTYPE));
                String lat= c.getString(c.getColumnIndex(LAT));
                String lng= c.getString(c.getColumnIndex(LNG));
                String sync= c.getString(c.getColumnIndex(SYNC));
                String synctime= c.getString(c.getColumnIndex(SYNCTIME));
                trxArrayList.add(new TrxHistory(id, uid, cardtype,interop, cardno, amount, balance, exp, timestamp, banklog, partnerdata, taptype, lat, lng, sync, synctime));
            } while(c.moveToNext());
        }

        return trxArrayList;
    }
}
