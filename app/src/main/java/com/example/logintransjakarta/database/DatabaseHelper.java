package com.example.logintransjakarta.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import static com.example.logintransjakarta.database.DatabaseContract.TABLE_TRXHISTORY;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static String DATABASE_NAME = "payment";
    private static final int DATABASE_VERSION = 1;
    private static final String SQL_CREATE_TABLE_TRXHISTORY = String.format("CREATE TABLE %s"
                    + " (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " %s CHAR(20) NOT NULL," +
                    " %s CHAR(4) NOT NULL," +
                    " %s CHAR(4) NOT NULL," +
                    " %s CHAR(20) NOT NULL," +
                    " %s CHAR(10) NOT NULL," +
                    " %s CHAR(10) NOT NULL," +
                    " %s CHAR(10) NOT NULL," +
                    " %s CHAR(20) NOT NULL," +
                    " %s CHAR(600) NOT NULL," +
                    " %s CHAR(150) NOT NULL," +
                    " %s CHAR(5) NOT NULL," +
                    " %s CHAR(40) NOT NULL," +
                    " %s CHAR(40) NOT NULL," +
                    " %s CHAR(5) NOT NULL," +
                    " %s CHAR(20) NOT NULL)",
            TABLE_TRXHISTORY,
            DatabaseContract.TrxHistoryColumns._ID,
            DatabaseContract.TrxHistoryColumns.UID,
            DatabaseContract.TrxHistoryColumns.CARDTYPE,
            DatabaseContract.TrxHistoryColumns.INTEROP,
            DatabaseContract.TrxHistoryColumns.CARDNO,
            DatabaseContract.TrxHistoryColumns.AMOUNT,
            DatabaseContract.TrxHistoryColumns.BALANCE,
            DatabaseContract.TrxHistoryColumns.EXP,
            DatabaseContract.TrxHistoryColumns.TIMESTAMP,
            DatabaseContract.TrxHistoryColumns.BANKLOG,
            DatabaseContract.TrxHistoryColumns.PARTNERDATA,
            DatabaseContract.TrxHistoryColumns.TAPTYPE,
            DatabaseContract.TrxHistoryColumns.LAT,
            DatabaseContract.TrxHistoryColumns.LNG,
            DatabaseContract.TrxHistoryColumns.SYNC,
            DatabaseContract.TrxHistoryColumns.SYNCTIME
    );

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_TRXHISTORY);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRXHISTORY);
        onCreate(db);
    }
}
