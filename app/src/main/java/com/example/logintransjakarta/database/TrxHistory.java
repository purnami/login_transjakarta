package com.example.logintransjakarta.database;

import android.os.Parcel;
import android.os.Parcelable;

public class TrxHistory implements Parcelable {
    private int id;
    private String uid;
    private String cardtype;
    private String interop;
    private String cardno;
    private String amount;
    private String balance;
    private String exp;
    private String timestamp;
    private String banklog;
    private String partnerdata;
    private String taptype;
    private String lat;
    private String lng;
    private String sync;
    private String synctime;

    public TrxHistory(int id, String uid, String cardtype, String interop, String cardno, String amount, String balance, String exp, String timestamp, String banklog, String partnerdata, String taptype, String lat, String lng, String sync, String synctime) {
        this.id = id;
        this.uid = uid;
        this.cardtype = cardtype;
        this.interop = interop;
        this.cardno = cardno;
        this.amount = amount;
        this.balance = balance;
        this.exp = exp;
        this.timestamp = timestamp;
        this.banklog = banklog;
        this.partnerdata = partnerdata;
        this.taptype = taptype;
        this.lat = lat;
        this.lng = lng;
        this.sync = sync;
        this.synctime = synctime;
    }

    protected TrxHistory(Parcel in) {
        id = in.readInt();
        uid = in.readString();
        cardtype = in.readString();
        interop = in.readString();
        cardno = in.readString();
        amount = in.readString();
        balance = in.readString();
        exp = in.readString();
        timestamp = in.readString();
        banklog = in.readString();
        partnerdata = in.readString();
        taptype = in.readString();
        lat = in.readString();
        lng = in.readString();
        sync = in.readString();
        synctime = in.readString();
    }

    public static final Creator<TrxHistory> CREATOR = new Creator<TrxHistory>() {
        @Override
        public TrxHistory createFromParcel(Parcel in) {
            return new TrxHistory(in);
        }

        @Override
        public TrxHistory[] newArray(int size) {
            return new TrxHistory[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }

    public String getInterop() {
        return interop;
    }

    public void setInterop(String interop) {
        this.interop = interop;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBanklog() {
        return banklog;
    }

    public void setBanklog(String banklog) {
        this.banklog = banklog;
    }

    public String getPartnerdata() {
        return partnerdata;
    }

    public void setPartnerdata(String partnerdata) {
        this.partnerdata = partnerdata;
    }

    public String getTaptype() {
        return taptype;
    }

    public void setTaptype(String taptype) {
        this.taptype = taptype;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getSynctime() {
        return synctime;
    }

    public void setSynctime(String synctime) {
        this.synctime = synctime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(uid);
        dest.writeString(cardtype);
        dest.writeString(interop);
        dest.writeString(cardno);
        dest.writeString(amount);
        dest.writeString(balance);
        dest.writeString(exp);
        dest.writeString(timestamp);
        dest.writeString(banklog);
        dest.writeString(partnerdata);
        dest.writeString(taptype);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(sync);
        dest.writeString(synctime);
    }
}

