package com.example.logintransjakarta.database;

import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String TABLE_TRXHISTORY = "trxHistory";
    public static final class TrxHistoryColumns implements BaseColumns {
        public static final String _ID = "_id";
        public static final String UID = "uid";
        public static final String CARDTYPE = "cardtype";
        public static final String INTEROP = "interop";
        public static final String CARDNO = "cardno";
        public static final String AMOUNT = "amount";
        public static final String BALANCE = "balance";
        public static final String EXP = "exp";
        public static final String TIMESTAMP = "timestamp";
        public static final String BANKLOG = "banklog";
        public static final String PARTNERDATA = "partnerdata";
        public static final String TAPTYPE = "taptype";
        public static final String LAT = "lat";
        public static final String LNG = "lng";
        public static final String SYNC = "sync";
        public static final String SYNCTIME = "synctime";


    }

}
