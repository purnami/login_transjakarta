package com.example.logintransjakarta;

import android.content.ContentValues;
import android.content.Context;
import android.os.Build;
import android.telecom.TelecomManager;
import android.util.Log;

import com.example.logintransjakarta.database.TrxHistory;
import com.example.logintransjakarta.database.TrxHistoryHelper;
import com.example.logintransjakarta.helper.ByteUtils;
import com.example.logintransjakarta.helper.Hex;
import com.example.logintransjakarta.helper.converter;
import com.example.logintransjakarta.helper.des;
import com.example.logintransjakarta.helper.util;

import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import static android.content.ContentValues.TAG;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.AMOUNT;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.BALANCE;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.BANKLOG;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.CARDNO;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.CARDTYPE;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.EXP;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.INTEROP;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.LAT;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.LNG;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.PARTNERDATA;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.SYNC;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.SYNCTIME;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.TAPTYPE;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.TIMESTAMP;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns.UID;
import static com.example.logintransjakarta.database.DatabaseContract.TrxHistoryColumns._ID;
import static com.example.logintransjakarta.helper.ErrorCode.ERR_CRC;
import static com.example.logintransjakarta.helper.ErrorCode.NOT_OK;
import static com.example.logintransjakarta.helper.ErrorCode.OK;
import static com.example.logintransjakarta.helper.params.ctype_bca;
import static com.example.logintransjakarta.helper.params.ctype_bni;
import static com.example.logintransjakarta.helper.params.ctype_bri;
import static com.example.logintransjakarta.helper.params.ctype_dki;
import static com.example.logintransjakarta.helper.params.ctype_mdr;

public class Transjakarta{
    private static TjData tjData=new TjData();
    public int outLogLen=0;
    des des;
//    TjData tjData=new TjData();

//    Transjakarta(){
//        tjData=new TjData();
//    }

    int loop=1;
//    des des;

    SocketThread socketThread;
    static int OFFSET_CMDRESPCODE =  5;
    static int OFFSET_CMD =  3;
    static byte[] login={(byte)0xAE,(byte)0X01};

    static final byte CAT_GENERAL = (byte)0x00;
    static final byte CAT_LOGIN = (byte)0x01;
    static final byte CAT_SENDDATA  = (byte)0x02;

    public static byte[] token;


    byte TAG_USERNAME = (byte) 0x01;
    byte TAG_PASSWORD = (byte) 0x02;
    byte TAG_LAT = (byte) 0x01;
    byte TAG_LNG = (byte) 0x02;
    static byte TAG_READERSN = (byte) 0x03;
    byte TAG_APPVER = (byte) 0x04;
    static byte TAG_DEVICETYPE = (byte) 0x05;
    byte TAG_TJ_TAPINTERVAL = (byte) 0x05;
    static byte TAG_MID = (byte) 0x06;
    static byte TAG_TID = (byte) 0x07;
    static byte TAG_SRVTIMESTAMP = (byte)0x08;
    static byte TAG_UPLOADINTERVAL = (byte) 0x09;
    static byte TAG_MAXRECUPLOAD = (byte) 0x10;
    static byte TAG_PORTTIMEOUT = (byte) 0x11;
    static byte TAG_TOKEN = (byte) 0x12;
    byte TAG_SETTLETIME = (byte) 0x13;
    static byte TAG_MAXPAYMENT = (byte) 0x13;
    byte TAG_RESPMSG = (byte) 0x14;
    static byte TAG_ARMADAID = (byte) 0x14;
    byte TAG_RFU = (byte)0x15;
    byte TAG_LOG = (byte) 0x16;
    static byte TAG_KEEPALIVE = (byte) 0x17;
    static byte TAG_SUBSIDIDURATION = (byte) 0x18;
    static byte TAG_TARIF_DATA = (byte) 0x22;
    static byte TAG_PAYMENTSCHEME = (byte) 0xAA;
    static byte TAG_SUMMARYINTERVAL = (byte) 0x35;
    static byte TAG_TIME_BASE = (byte) 0x36;
    byte TAG_CASE_BASE = (byte) 0x37;
    static byte TAG_ACTIVEPAYMENTRULE = (byte) 0x38;
    static byte TAG_TJ_TERMINALCODE = (byte) 0x39;
    static byte TAG_TJPASSWORD = (byte) 0x40;
    static byte TAG_TJ_URL = (byte) 0x41;
    byte TAG_NOBODY = (byte) 0x44;
    byte TAG_TRXSUMMARY = (byte) 0x49;
    byte TAG_TRXTYPE = (byte) 0x50;
    byte TAG_TJCARDTYPE = (byte) 0x52;
    byte TAG_CARDINTEROP = (byte) 0x53;
    static byte TAG_TJSETTING = (byte) 0xAB;
    int MAX_TRIAL=3;
    int TJ_DKI = 1;
    int TJ_BNI = 2;
    int TJ_BRI = 3;
    int TJ_MANDIRI = 4;
    int TJ_BCA = 5;
    int TJ_MEGA = 6;
    int TJ_JAKLINGKO_DKI = 7;
    int TJ_JAKLINGKO_BNI = 8;
    int TJ_SINGLE_CARD = 9;
    int TJ_PENGEMUDI = 10;
    int TJ_JAKLINGKO_MANDIRI = 11;
    int TJ_JAKLINGKO_BRI = 12;
    int TJ_JAKLINGKO_BCA = 13;
    int TJ_DKI_GRATIS = 14;
    int TJ_BNI_GRATIS = 15;
    int TJ_BRI_GRATIS = 16;
    int TJ_MDR_GRATIS = 17;
    int TJ_BCA_GRATIS = 18;
    byte _TAP_OUT_ = (byte) 0x00;
    byte _TAP_IN_ = (byte) 0x01;





    String jaklingko="________________________________01______________________________________________________________________________________________";
    String regular=  "________________________________00______________________________________________________________________________________________";

    static int OFFSET_CMDDATA =  7;

    static int TAG_SIZE =  1;
    static int LEN_SIZE =  2;

    final byte ERR_INVALID_USER_PASSWORD = (byte) 0x01;
    final byte ERR_INVALID_SN = (byte) 0x02;
    final byte ERR_INVALID_FORMAT = (byte) 0x03;
    final byte ERR_USER_INACTIVE = (byte) 0x04;
    final byte ERR_INVALID_TOKEN = (byte) 0x05;
    final byte ERR_INVALID_MID = (byte) 0x06;
    final byte ERR_INVALID_TID = (byte)0x07;
    final byte ERR_INVALID_TRXLOG = (byte) 0x08;
    final byte ERR_INACTIVE_MERCHANT = (byte) 0x09;
    final byte ERR_INACTIVE_SITE = (byte) 0x0A;
    final byte ERR_INACTIVE_TERMINAL = (byte)0x0B;
    final byte ERR_EXPIRED_MERCHANT = (byte) 0x0C;
    final byte ERR_SERVER_ERROR = (byte) 0xFF;
    final byte TJ = (byte) 0x01;
    final byte STI = (byte) 0x02;
    private static byte[] response;
    private static int resLen;
    int offset;
    byte[] dst;

    String ip="119.110.72.218";
//    String ip="192.168.11.95";
    int port=10127;

    Context ctx;
    private TrxHistoryHelper trxHistoryHelper;
    ArrayList<TrxHistory> trxHistory;

    DevInfo.bank_device devInfo;
    private int lastSelectColCount=0;

    public Transjakarta(Context ctx) {
        this.ctx=ctx;
        devInfo=new DevInfo.bank_device();
        des=new des();
    }

    public void STISendData(byte[] data) {
        trxHistoryHelper=TrxHistoryHelper.getInstance(ctx);
        trxHistoryHelper.open();

        socketThread=new SocketThread();
        new Thread(new Runnable() {
            @Override
            public void run() {
                socketThread.connect(ip, port);
                byte[] resp = socketThread.sendData(data);
                socketThread.close();
                Log.d("response", ""+Hex.bytesToASCIIString(resp));
                CheckLogonResponse_TLV(resp, resp.length);
                String datetime = util.Datenow("yyyyMMddHHmmss");
                byte[] dateBcd = Hex.hexStringToByteArray(datetime);
                tj_trxsummary(dateBcd, dateBcd.length);
                TransactionSummaryTLV(new byte[0], 0);
                Task_DataUpload();
            }
        }).start();
    }
    int TransactionSummaryTLV(byte[] outData,int outLen)
    {
        int total = TjSummary.MANDIRI+
                        TjSummary.JAKLINGKO_MANDIRI+
                        TjSummary.DKI+
                        TjSummary.JAKLINGKO_DKI+
                        TjSummary.BNI+
                        TjSummary.JAKLINGKO_BNI+
                        TjSummary.BRI+
                        TjSummary.JAKLINGKO_BRI+
                        TjSummary.BCA+
                        TjSummary.JAKLINGKO_BCA;
        dst=new byte[1024];
        byte[] result=new byte[1024];
        byte[] temp=new byte[4];
        byte[] devsn=new byte[20];

        int i = 0;
        offset=0;
        byte STX = 0x02;
        byte[] jaklingko_summary=new byte[]{(byte) 0xAB,0X04};

        String summarydata=String.format("{\"grand_total\":0,\"total_pending\":0,\"total_transaction\":%s,\"details\":["+
                "{\"card_type_id\":\"1\",\"card_type_name\":\"TJ_DKI\",\"total\":%s},"+
                "{\"card_type_id\":\"2\",\"card_type_name\":\"TJ_BNI\",\"total\":%s},"+
                "{\"card_type_id\":\"3\",\"card_type_name\":\"TJ_BRI\",\"total\":%s},"+
                "{\"card_type_id\":\"4\",\"card_type_name\":\"TJ_MANDIRI\",\"total\":%s},"+
                "{\"card_type_id\":\"5\",\"card_type_name\":\"TJ_BCA\",\"total\":%s},"+
                "{\"card_type_id\":\"7\",\"card_type_name\":\"TJ_JAKLINGKO_DKI\",\"total\":%s},"+
                "{\"card_type_id\":\"8\",\"card_type_name\":\"TJ_JAKLINGKO_BNI\",\"total\":%s},"+
                "{\"card_type_id\":\"11\",\"card_type_name\":\"TJ_JAKLINGKO_MANDIRI\",\"total\":%s},"+
                "{\"card_type_id\":\"12\",\"card_type_name\":\"TJ_JAKLINGKO_BRI\",\"total\":%s},"+
                "{\"card_type_id\":\"13\",\"card_type_name\":\"TJ_JAKLINGKO_BCA\",\"total\":%s}"+
                "]}",total,TjSummary.DKI,TjSummary.BNI,TjSummary.BRI,TjSummary.MANDIRI,TjSummary.BCA,TjSummary.JAKLINGKO_DKI,TjSummary.JAKLINGKO_BNI,TjSummary.JAKLINGKO_MANDIRI,TjSummary.JAKLINGKO_BRI,TjSummary.JAKLINGKO_BCA);

        dst[offset++]=STX;

        offset+=2;

        Log.d("offset", ""+offset);

        System.arraycopy(jaklingko_summary, 0, dst, offset, jaklingko_summary.length);
        Log.d("dst", ""+Hex.bytesToHexString(dst));

        offset+=jaklingko_summary.length;
        Log.d("offset", ""+offset);

        dst=ComposeTLV(TAG_TOKEN,Deduct.token,Deduct.token.length, dst);

        dst=ComposeTLV(TAG_MID,devInfo.MID,devInfo.MID.length,dst);
        Log.d("offset", ""+offset);
        Log.d("dst", ""+Hex.bytesToHexString(dst));

        dst=ComposeTLV(TAG_TID,devInfo.TID,devInfo.TID.length,dst);

        dst=ComposeTLV(TAG_TRXSUMMARY,summarydata.getBytes(),summarydata.length(),dst);

        Log.d("devInfo.SN", ""+Hex.bytesToHexString(devInfo.SN));
        System.arraycopy(devInfo.SN, 0, devsn, 20-devInfo.lenSN, devInfo.lenSN);

        dst=ComposeTLV(TAG_READERSN,devsn,20,dst);

        temp = ByteUtils.int2bytes(offset-3);
        byte[] length = Hex.IntegerToByteArray(offset - 3);

        System.arraycopy(length,2,dst,1,2);

        dst[offset++]=calculateLRC(dst, 1, offset - 1);

        result = Hex.subByte(dst, 0, offset);

        String ip="119.110.72.218";
        int port=10127;
        socketThread.connect(ip, port);
        byte[] resp = socketThread.sendData(result);
        socketThread.close();
        Log.d("response", ""+Hex.bytesToASCIIString(resp));

        return i;
    }

    private static byte calculateLRC(byte[] bytes,int offset,int length) {
        byte LRC = 0;
        for (int i = offset; i <length+offset ; i++) {
            LRC ^= (bytes[i] & 0xFF);
        }
        return LRC;
    }

    byte[] ComposeTLV(byte TAG, byte[] data, int datalen, byte[] dst)
    {

        System.arraycopy(new byte[]{TAG}, 0, dst, offset, 1);
        offset+=1;

        System.arraycopy(ByteUtils.int2bytes(datalen), 2, dst, offset, 2);
        offset+=2;

        System.arraycopy(data, 0, dst, offset, datalen);
        offset+=datalen;

        return dst;
    }

    public int CheckLogonResponse_TLV(byte[] resp, long resplen)
    {
        byte crc;
        int taglen;
        byte[] tagData = new byte[1024];
        byte[] paramData= new byte[100];
        int paramlen;
        byte[] temp= new byte[4];
        byte[] devsn= new byte[20];
        long number;
        int result;
        byte[] bPaymentScheme= new byte[512];
        byte[] timeconv= new byte[7];
        int schemelength;
        byte[] bcadata = new byte[31];
        byte[] bcarsp= new byte[31];
        int ibcadata = 0;
        byte[] tjdata_username=new byte[100];
        byte[] tjdata_password=new byte[100];

        crc=XORing(resp, (int) (resplen-1), 1);

        DebugString("crc", ""+ Hex.byteToHexString(crc));
        DebugString("resp[(int)resplen-1]", ""+Hex.byteToHexString(resp[(int)resplen-1]));
        if(crc!=resp[(int)resplen-1])
        {
            DebugString(TAG, "Incorrect CRC");
            return ERR_CRC;
        }
        result = pcd_checkresponsecode(resp,(int) resplen);
        DebugString("result", ""+result);
        if(result!=OK)
        {
            return result;
        }

        tagData=FindTag(TAG_READERSN, resp,(int) resplen-7-1,OFFSET_CMDDATA);
        taglen=tagData.length;
        DebugString("taglen", ""+taglen);
        devInfo.lenSN=taglen;
        System.arraycopy(Hex.subByte(tagData, 0, taglen), 0, devInfo.SN, 0, taglen);
        DebugStringHex("SN", devInfo.SN);

        tagData=FindTag(TAG_DEVICETYPE, resp,(int) resplen-7-1,OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, temp, 2, taglen);
        DebugStringHex("temp", temp);

        tagData=FindTag(TAG_MID,resp,(int) resplen-7-1,OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, devInfo.MID, 0, taglen);
        DebugStringHex("MID", devInfo.MID);

        tagData=FindTag( TAG_TID,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, devInfo.TID, 0, taglen);
        DebugStringHex("TID", devInfo.TID);

        tagData= FindTag( TAG_SRVTIMESTAMP,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;

        if(Deduct.fLoginSuccess==true)
        {
            System.arraycopy(tagData, 0, devInfo.reftime, 0, taglen);
            DebugStringHex("reftime", devInfo.reftime);

            devInfo.doUpdatetime=true;
        }

        tagData= FindTag( TAG_UPLOADINTERVAL,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        number= ByteUtils.bytesToInt(tagData,0);
        DebugString("number", ""+number);
        System.arraycopy(tagData, 0, Deduct.bUploadWindow, 0, taglen);
        DebugStringHex("deduct_bUploadWindow", Deduct.bUploadWindow);
        Deduct.lUploadWindow = number;
        Deduct.lUploadWindow*=1000;
        DebugString("deduct_lUploadWindow", ""+Deduct.lUploadWindow);

        tagData= FindTag( TAG_MAXRECUPLOAD,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        Deduct.maxrecordupload = tagData[0];
        DebugString("deduct_maxrecordupload", ""+Hex.byteToHexString(Deduct.maxrecordupload));

        tagData= FindTag( TAG_PORTTIMEOUT,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, temp, 2, taglen);
        DebugStringHex("temp", temp);
        Deduct.port_to=ByteUtils.bytesToInt(temp,0);
        DebugString("deduct_port_to", ""+Deduct.port_to);

        tagData= FindTag( TAG_TOKEN,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, Deduct.token, 0, taglen);
        DebugStringHex("deduct_token", Deduct.token);
        Deduct.lenToken = taglen;

        tagData= FindTag( TAG_MAXPAYMENT,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, Deduct.baMaxPay, 4-taglen, taglen);
        DebugStringHex("deduct_baMaxPay", Deduct.baMaxPay);
        Deduct.iMaxPay=ByteUtils.bytesToInt(Deduct.baMaxPay,0);

        tagData= FindTag( TAG_ARMADAID,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;

        tagData= FindTag( TAG_SUMMARYINTERVAL,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, temp, 2, taglen);
        DebugStringHex("temp", temp);

        tagData= FindTag( TAG_KEEPALIVE,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, Deduct.AliveWindow, 0, taglen);
        DebugStringHex("deduct_AliveWindow", Deduct.AliveWindow);
        System.arraycopy(tagData, 0, temp, 2, taglen);
        DebugStringHex("temp", temp);
        Deduct.lAliveWindow=ByteUtils.bytesToInt(temp,0);

        tagData= FindTag( TAG_SUBSIDIDURATION,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, Deduct.baSubsidiDuration, 0, taglen);
        DebugStringHex("baSubsidiDuration", Deduct.baSubsidiDuration);
        Deduct.lSubsidiDuration=(Deduct.baSubsidiDuration[1]+Deduct.baSubsidiDuration[0]*60)*60;

        tagData= FindTag( TAG_TARIF_DATA,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        System.arraycopy(tagData, 0, Deduct.baAmount, 4-taglen, taglen);
        DebugStringHex("deduct_baAmount", Deduct.baAmount);
        Deduct.iAmount = ByteUtils.bytesToInt(Deduct.baAmount, 0);

        tagData= FindTag( TAG_PAYMENTSCHEME,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        schemelength=taglen;

        if(schemelength>0)
        {
            System.arraycopy(tagData, 0, bPaymentScheme, 0, schemelength);
            taglen=FindSCPITag( TAG_ACTIVEPAYMENTRULE,bPaymentScheme,schemelength,tagData);
            if(taglen>0)
            {
//                deduct_rule=tagData[0];
            }
            else
            {
//                deduct.rule=0;
            }
//            DebugTariffRule("deduct.rule",&deduct.rule);

//            DebugPrintString("Get time base scheme");
            tagData= FindTag( TAG_TIME_BASE,bPaymentScheme,schemelength,0);
            taglen=tagData.length;
            DebugStringHex("tagData", tagData);
            DebugString("taglen", ""+taglen);
            if(taglen>0)
            {
                //tagdata[taglen++]='|';//2019-11-19, vivo, added to help time based price rule
//                byte delimiter_multitarif = '|';
//                tagData[taglen++]=delimiter_multitarif;//2019-11-19, vivo, added to help time based price rule
//                memset(deduct.timebase_price,0x00,sizeof(deduct.timebase_price));
//                System.arraycopy(tagData, 0, deduct.timebase_price, 0, taglen);
//                memcpy(deduct.timebase_price,tagData,taglen);
//                DebugHex("deduct.timebase_price",deduct.timebase_price,taglen);
//                deduct.lentimebase_price=taglen;

            }
            else
            {
//                deduct.lentimebase_price=0;
            }
//            DebugPrintString("Get card base scheme");
//            taglen=FindTag( TAG_CASE_BASE,bPaymentScheme,schemelength,tagData);
//            deduct.lencardbase_price=taglen;//this flag is needed to be set because it will be check in ProcessMultiTarifRule
//            parse_cardbased_promoscheme(tagData,taglen);

        }

        tagData= FindTag( TAG_TJSETTING,resp,(int) resplen-7-1, OFFSET_CMDDATA);
        taglen=tagData.length;
        DebugStringHex("tagData", tagData);
        DebugString("taglen", ""+taglen);
        schemelength=taglen;

        if(schemelength>0) {
            DebugPrintString("Parsing tj setting");
            System.arraycopy(tagData, 0, bPaymentScheme, 0, schemelength);

            DebugPrintString("Get tariff rule");
            tagData =  FindTag( TAG_TJ_TERMINALCODE, bPaymentScheme, schemelength, 0);
            taglen = tagData.length;
            if (taglen > 0) {
                Arrays.fill(tjData.username, (byte) 0);
                System.arraycopy(tagData, 0, tjData.username, 0, taglen);
                tjData.username=Hex.subByte(tjData.username, 0, tjData.username.length);
                DebugString("tjusername", ""+Hex.bytesToASCIIString(tjData.username));
            }
            tagData =  FindTag( TAG_TJPASSWORD, bPaymentScheme, schemelength, 0);
            taglen = tagData.length;
            if (taglen > 0) {
                Arrays.fill(tjData.password, (byte) 0);
                System.arraycopy(tagData, 0, tjData.password, 0, taglen);
                tjData.password=Hex.subByte(tjData.password, 0, tjData.password.length);
                DebugString("tjpassword", ""+Hex.bytesToASCIIString(tjData.password));
            }

            tagData =  FindTag( TAG_TJ_URL, bPaymentScheme, schemelength, 0);
            DebugStringHex("tagData", tagData);
            Log.d("taglen", "" + taglen);
            if (taglen > 0) {
                tj_parseip(tagData, taglen);
            }
        }

        return OK;
    }

    private int tj_parseip(byte[] addr, int len)
    {
        DebugPrintString("tj_parseip");

        String url=Hex.bytesToASCIIString(addr);
        String[] split = url.split("/");

        String ip = null;
        String port =null;
        for(int i=0;i<split.length;i++){
            Log.d("split", ""+i+"  "+split[i]);
            if(i==2){
                String ipport=split[i];
                String[] split2=ipport.split(":");
                for(int j=0;j<split2.length;j++){
                    Log.d("split2", ""+j+"  "+split2[j]);
                    ip=split2[0];
                    port=split2[1];
                }
            }
        }


        tjData.ip = ip;
        tjData.port = Integer.parseInt(port);
        Log.d("ip", ""+tjData.ip);
        Log.d("port", ""+tjData.port);

        return 0;
    }

    int tj_login()
    {
        DebugPrintString("tj_login");
        char[] name=new char[100];
        byte[] respon=new byte[100];
        char[] pw =new char[100];
        int iRet;
        char[] value=new char[200];

        int lenvalue;
        int lenvalue_2;

        DebugStringHex("tjusername", tjData.username);
        String username=Hex.bytesToASCIIString(tjData.username);
        String password=Hex.bytesToASCIIString(tjData.password);
        String sqlcmd = String.format("{\"action\":\"login_bus_validator\",\"terminal_code\":\"%s\",\"password\":\"%s\"}", username.replace(".",""), md5(password.replace(".","")));

        String ip=tjData.ip;
        int port=tjData.port;
        socketThread=new SocketThread();
        socketThread.connect(ip, port);
        byte[] resp = socketThread.sendData(sqlcmd.getBytes());
        DebugString("responsetjlogin", ""+Hex.bytesToASCIIString(resp));

        String code = jsonGetValue(resp, "code");
        if(code!=null){
            if(code.equals("0")){
                tjData.token=jsonGetValue(resp, "token");
                tjData.direction_id=jsonGetValue(resp, "direction_id");
                tjData.direction_name=jsonGetValue(resp, "direction_name");
                tjData.loginSuccess=true;
                return OK;
            }else{
                return NOT_OK;
            }
        }
        else {
            return NOT_OK;
        }
    }

    static byte pcd_checkresponsecode(byte[] resp, int resplen)
    {

        byte[] respcode = new byte[2];
        byte[] command = new byte[2];

        System.arraycopy(resp,OFFSET_CMDRESPCODE,respcode,0,2);
        System.arraycopy(resp,OFFSET_CMD,command,0,2);
        if(Arrays.equals(login,command)==true)
        {
            switch(respcode[0])
            {
                case CAT_GENERAL:
                    switch(respcode[1])
                    {
                        case OK:
                            token=FindTag(TAG_TOKEN,resp,resplen,OFFSET_CMDDATA);
                            return OK;
                    }
                    return respcode[1];
                case CAT_LOGIN:
                case CAT_SENDDATA:
                    return respcode[1];

                default:
                    //Log.d(TAG,"unknown response code category");
                    break;
            }
        }
        switch(respcode[0])
        {
            case CAT_GENERAL:
                switch(respcode[1])
                {
                    case (byte)0x00 : return OK;
                }
                break;
            case CAT_LOGIN:
                switch(respcode[1])
                {
                    case (byte)0x00 :
                        token=FindTag(TAG_TOKEN,resp,resplen,0);

                        return OK;
                }
                return respcode[1];
            case CAT_SENDDATA:
                return respcode[1];

            default:
                //Log.d(TAG,"unknown response code category");
                break;
        }

        return (byte)0xFF;
    }

    static byte[] FindTag(byte tag, byte[] data, int lenData, int offset)
    {

        int idx=offset;
        byte[] outdata;
        int iLen=0;
        byte[] length = new byte[4];
        Log.d("lenData", ""+lenData+"  idx : "+idx);
        while(lenData>idx)
        {
            Log.d("data[idx]", ""+data[idx]+"  idx:"+idx+"   tag:"+tag);
            if(data[idx]!=tag)
            {
                //Log.d(TAG,"Different TAG, continue search tag");
                System.arraycopy(data,idx+1,length,2,2);
                iLen = converter.ByteArrayToInt(length,0,4,converter.BIG_ENDIAN);
                idx+=TAG_SIZE+LEN_SIZE+iLen;

            }
            else
            {
                //Log.d(TAG,"Same TAG, return tag data");
                System.arraycopy(data,idx+1,length,2,2);
                Log.d("length", ""+Hex.bytesToHexString(length));
                iLen = converter.ByteArrayToInt(length,0,4,converter.BIG_ENDIAN);
                Log.d("iLen", ""+iLen);
                outdata= new byte[iLen];
                System.arraycopy(data,idx+TAG_SIZE+LEN_SIZE,outdata,0,iLen);
                Log.d("outdata", ""+Hex.bytesToHexString(outdata));
                return outdata;
            }
        }
        return null;
    }

    static int FindSCPITag(byte tag, byte[] data, int lenData, byte[] outdata)
    {

        int idx=0;

        while(lenData>idx)
        {

            if(data[idx]!=tag)
            {

                idx+=2+data[idx+1];
            }
            else
            {
                System.arraycopy(data, idx+2, outdata, 0, data[idx+1]);
                return data[idx+1];

            }
        }
        return 0;
    }

    public static void setResponse(byte[] tempbuffer, int length) {
        response=tempbuffer;
        resLen=length;
    }

    public static byte XORing(byte[] src, int len, int start)
    {
        byte tmp;
        int offset;
        offset=start;
        tmp=0;
        Log.d("XORing", ""+ Hex.bytesToHexString(src));
        while(offset<len)
        {
            tmp ^= src[offset];
            offset++;
        }
        return tmp;
    }

    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    String jsonGetValue(byte[] source, String key)
    {
        String data=Hex.bytesToASCIIString(source);
        String[] splitChar = data.split(":");
        String outValue = null;
        for(int i=0;i<splitChar.length;i++){
            DebugString("splitchar", ""+splitChar[i]);
            if(splitChar[i].contains(key)){
                outValue=splitChar[i+1].replace("\"","");
                outValue=outValue.replace("}","");
                outValue=outValue.replace("{","");
                String[] splitValue = outValue.split(",");
                if(splitValue.length>0){
                    outValue=splitValue[0];
                }
            }
        }
        DebugString("outvalue", ""+outValue);
        return outValue;
    }

    int tj_trxsummary(byte[] datetime,int datetimelen)
    {
        DebugPrintString("tj_trxsummary");
        byte[] tjcardtype=new byte[2+1];
        byte[] temp=new byte[40];
        byte[] strDateBCD=new byte[20];
        char[] sqlcmd=new char[500];
        int retry=0;
        int iResp;
        int offset;
        char[] resp=new char[100];
        char[] value=new char[200];

        int lenvalue;
        int lenvalue_2;
        int code=-1;

        trxHistoryHelper=TrxHistoryHelper.getInstance(ctx);

        trxHistoryHelper.open();
//        ContentValues values=new ContentValues();
//        values.put(UID, "0000000F036C88");
//        values.put(CARDTYPE, "02");
//        values.put(INTEROP, "1110");
//        values.put(CARDNO, "5710260920170016");
//        values.put(AMOUNT, "00000001");
//        values.put(BALANCE, "00000001");
//        values.put(EXP, "0001");
//        values.put(TIMESTAMP, "12082021172928");
//        values.put(BANKLOG, "010101571026092017001600000090000000000000000030010510000000340000048500000000042C000057E28CEFBDD5");
////        values.put(PARTNERDATA, ""+partnerdata);
//        values.put(PARTNERDATA, "000000002817110C000000000000000000000000000000040100000000FFFFFF0530304445564A414B3100000000000000000000000000000000000000000000");
//        values.put(TAPTYPE, "01");
//        values.put(LAT, "-6.1282591");
//        values.put(LNG, "106.7352699");
//        values.put(SYNC, "0");
//        values.put(SYNCTIME, "12082021173131");
//
//        trxHistoryHelper.insert(values);

        DebugHex("datetime",datetime,datetimelen);
        retry=0;

        strDateBCD=datetime;
        if(!tjData.loginSuccess)
        {
           while(retry<MAX_TRIAL)
           {
            int iRet=tj_login();
            if(iRet==OK)
            break;
            else if(retry>MAX_TRIAL)
                return NOT_OK;
            else
                retry++;
            }
        }
        send_banksummary(ctype_mdr,TJ_MANDIRI, strDateBCD,regular,regular.length(), new int[]{TjSummary.MANDIRI});

        send_banksummary(ctype_mdr,TJ_JAKLINGKO_MANDIRI, strDateBCD,jaklingko,jaklingko.length(), new int[]{TjSummary.JAKLINGKO_MANDIRI});

        send_banksummary(ctype_dki,TJ_DKI, strDateBCD,regular,regular.length(), new int[]{TjSummary.DKI});

        send_banksummary(ctype_dki,TJ_JAKLINGKO_DKI, strDateBCD,jaklingko,jaklingko.length(), new int[]{TjSummary.JAKLINGKO_DKI});

        send_banksummary(ctype_bni,TJ_BNI, strDateBCD,regular,regular.length(), new int[]{TjSummary.BNI});

        send_banksummary(ctype_bni,TJ_JAKLINGKO_BNI, strDateBCD,jaklingko,jaklingko.length(), new int[]{TjSummary.JAKLINGKO_BNI});

        send_banksummary(ctype_bri,TJ_BRI, strDateBCD,regular,regular.length(), new int[]{TjSummary.BRI});

        send_banksummary(ctype_bri,TJ_JAKLINGKO_BRI, strDateBCD,jaklingko,jaklingko.length(), new int[]{TjSummary.JAKLINGKO_BRI});

        send_banksummary(ctype_bca,TJ_BCA, strDateBCD,regular,regular.length(), new int[]{TjSummary.BCA});

        send_banksummary(ctype_bca,TJ_JAKLINGKO_BCA, strDateBCD,jaklingko,jaklingko.length(), new int[]{TjSummary.JAKLINGKO_BCA});
        socketThread.close();

        return OK;
    }
//    int send_banksummary(int cardtype,int tjcardtype,byte[] strDateBCD,byte[] partnerdata,int partnerdatalen,int[] oCounter)
    int send_banksummary(int cardtype,int tjcardtype,byte[] strDateBCD,String partnerdata,int partnerdatalen,int[] oCounter)

//    int send_banksummary(int cardtype,byte[] strDateBCD)
    {

        String sqlcmd;
//        byte[] resp=new byte[100];
        char[] value=new char[200];
        byte[] temp=new byte[40];
        int retry=MAX_TRIAL;
        int iResp;
        int offset;
        int lenvalue;
        int lenvalue_2;
        int code=-1;
        iResp=0;

        sqlcmd=String.format("WHERE "+TIMESTAMP+" LIKE '"+Hex.bytesToHexString(strDateBCD)+"' AND "+CARDTYPE+"='"+cardtype+"' AND "+TAPTYPE+"='01' AND "+PARTNERDATA+" LIKE '"+partnerdata+"' LIMIT 0, 49999");

        while(retry>0)
        {
            iResp= sqlfx_exec_select(sqlcmd);
            DebugString("iResp", ""+iResp);
            if(iResp < 0)
            {
                DebugString("DB", "Failed to select txnRecord");
                retry--;
            }
            else
            {
                retry=0;
            }
        }

        temp=Arrays.copyOfRange(strDateBCD, 0, 4);
        byte[] year = Arrays.copyOfRange(temp, 0, 2);
        byte[] mount = Arrays.copyOfRange(temp, 2, 3);
        byte[] day = Arrays.copyOfRange(temp, 3, 4);
        Log.d("temp", ""+Hex.bytesToHexString(temp));

        String username=Hex.bytesToASCIIString(tjData.username).replace(".","");
        sqlcmd=String.format("{\"action\":\"recap_transaction\",\"token\":\"%s\",\"transaction\":{\"terminal_code\":\"%s\",\"shelter_code\":\"%s\",\"card_type\":\"%s\",\"trx_date\":\"%s-%s-%s\",\"trx_count\":\"%s\"}}",
                tjData.token,username, username, tjcardtype,Hex.bytesToHexString(year),Hex.bytesToHexString(mount),Hex.bytesToHexString(day),iResp);
        DebugString("sqlcmd", ""+sqlcmd);

        byte[] resp = socketThread.sendData(sqlcmd.getBytes());
        DebugString("responsetjsenddata", ""+Hex.bytesToASCIIString(resp));

        String outValue = jsonGetValue(resp, "code");
        DebugString("outvalue banksummary", ""+outValue);
        if(outValue.equals("0")){
            return OK;
        }else {
            return NOT_OK;
        }

    }

    private int sqlfx_exec_select(String sqlcmd) {
        trxHistory = trxHistoryHelper.getAllData(sqlcmd);
//        for(int i=0;i<trxHistory.size();i++){
//            Log.d("trxHistory", "id "+i+" "+trxHistory.get(i).getId());
//            Log.d("trxHistory", "timestamp "+i+" "+trxHistory.get(i).getTimestamp());
//            Log.d("trxHistory", "cardtype "+i+" "+trxHistory.get(i).getCardtype());
//            Log.d("trxHistory", "partnerdata "+i+" "+trxHistory.get(i).getPartnerdata());
//            Log.d("trxHistory", "sync "+i+" "+trxHistory.get(i).getSync());
//        }
        return trxHistory.size();

    }


    byte[] payment_selecttrxrecord_TLV( /*out*/int trxtap/*out*/ ,byte[] outLog, int trxNo)
    {
        DebugPrintString("payment_selecttrxrecord_TLV");

        String strId= new String();
        byte[] strUID=new byte[20];
        byte[] strCardType=new byte[10];
        byte[] strCardNo=new byte[20];
        byte[] strBalance=new byte[10];
        byte[] strExp=new byte[10];
        byte[] strBCDtime=new byte[20];
        byte[] strBankLog=new byte[400];
        byte[] strPartnerData=new byte[256];
        byte[] strTapcode=new byte[10];
        byte[] strLat=new byte[40];
        byte[] strLng=new byte[40];
        byte[] strDeduct=new byte[8];
        byte[] strInterop=new byte[8];

        offset=0;
        int tagloglen_offset;
        byte[] output=new byte[1024];
        byte[] jaklingko_uploadtrxdata=new byte[]{(byte) 0xAB, 0x02};
//
        byte[] stilog=new byte[1024];
        int stiloglen;
        byte[] crc=new byte[4];
        byte[] stilogencrypt=new byte[1024];
        int stilogencryptlen;
        byte[] seed=new byte[4];
        int i;
        byte[] uid=new byte[8];
        byte[] temp=new byte[768];
        byte[] oprlog=new byte[512];
        int oprloglen;
        int trxid = 0;
        int tapcode;
        byte[] gpstemp=new byte[50];
//        char prefix[2]="2D";
        byte[] baTagLen=new byte[4];
//        BYTE empty[4];
        int deductamount;
        int tjtype;
        byte[] bInterop=new byte[2];
        int len_partner;
//
        String sqlcmd = String.format("WHERE " + SYNC + " IS '0' LIMIT 1");

        Log.d("sync is", "0");
        int iResp = sqlfx_exec_select(sqlcmd);
        if(iResp < 0)
        {
            Log.d("DB", "Failed to select txnRecord");
//            SetDBIdle();
//            return NOT_OK;
        }
        if(iResp==0)
        {
            DebugPrintString("No new record found, try searching record that previously failed to be sync");
            sqlcmd = String.format("WHERE " + SYNC + " IS NOT '1' LIMIT 1");

            iResp = sqlfx_exec_select(sqlcmd);
            if(iResp <= 0)
            {
                DebugPrintString("DB: Failed to select txnRecord or No record");
//                SetDBIdle();
//                return NOT_OK;
            }

        }

        DebugString("sqlcmd",sqlcmd);
        DebugPrintString("Start reading record data");


        strId= sqlfx_readout_select(_ID, sqlcmd);
        DebugString("strId",strId);
        trxid=Integer.parseInt(strId);

        strUID=Hex.hexStringToBytes(sqlfx_readout_select(UID, sqlcmd));
        DebugStringHex("strUID",strUID);
        byte[] card = new byte[1];
        card[0]=Hex.hexStringToByte(sqlfx_readout_select(CARDTYPE, sqlcmd));
        strCardType=card;
        TjData.card_type= Hex.bytesToHexString(strCardType);
        DebugStringHex("strCartType",strCardType);

        strDeduct=Hex.hexStringToByteArray(sqlfx_readout_select(AMOUNT, sqlcmd));
        DebugStringHex("strDeduct", strDeduct);

        strCardNo=Hex.hexStringToByteArray(sqlfx_readout_select(CARDNO, sqlcmd));
        DebugStringHex("strCardNo", strCardNo);

        strBalance=Hex.hexStringToByteArray(sqlfx_readout_select(BALANCE, sqlcmd));
        DebugStringHex("strBalance", strBalance);

        strExp=Hex.hexStringToByteArray(sqlfx_readout_select(EXP, sqlcmd));
        DebugStringHex("strExp", strExp);

        strBCDtime=Hex.hexStringToByteArray(sqlfx_readout_select(TIMESTAMP, sqlcmd));
        DebugStringHex("strBCDtime", strBCDtime);

        strBankLog=sqlfx_readout_select(BANKLOG, sqlcmd).getBytes();
        DebugStringHex("strBankLog", strBankLog);

        strPartnerData=Hex.hexStringToByteArray(sqlfx_readout_select(PARTNERDATA, sqlcmd));
        DebugStringHex("strPartnerData", strPartnerData);

        len_partner=strPartnerData.length;
        DebugString("strPartnerData", ""+len_partner);

        strTapcode=Hex.hexStringToByteArray(sqlfx_readout_select(TAPTYPE, sqlcmd));
        DebugStringHex("strTapcode", strTapcode);

        strLat=sqlfx_readout_select(LAT, sqlcmd).getBytes();
        DebugStringHex("strLat", strLat);

        strLng=sqlfx_readout_select(LNG, sqlcmd).getBytes();
        DebugStringHex("strLng", strLng);

        strInterop=Hex.hexStringToByteArray(sqlfx_readout_select(INTEROP, sqlcmd));
        DebugStringHex("strInterop", strInterop);

//#ifdef _TOB_
//        DebugPrintString("Process TOB Protocol");
////
//        TjData.bcdtimestamp=String.format("%s",Hex.bytesToASCIIString(strBCDtime));
//        DebugString("TjData.bcdtimestamp", TjData.bcdtimestamp);
//
//        System.arraycopy(strDeduct, 0, seed, 0, strDeduct.length);

////        hexstring_to_hexarray(strDeduct,strlen(strDeduct),seed);//get deduct amount
//        deductamount=ByteUtils.bytesToInt(seed, 0);
//        Log.d("deductAmount", ""+deductamount);
////        deductamount=dword2int(seed);//get amount value in integer
////        //DebugStringLen("strDeduct",dbbuffer.strDeduct,strlen(dbbuffer.strDeduct));
//
//        output=strBalance;
////        hexstring_to_hexarray(strBalance,strlen(strBalance),&output[4]);
//        tj_setfares(Deduct.normalfare,seed,output, new byte[]{output[4]});
//        tj_latlng(strLat,strLat.length, strLng,strLng.length);
////        hexstring_to_hexarray(strCardType,strlen(strCardType),seed);//get card type
//        tj_setcardno(strCardNo,strlen(strCardNo));
//        tj_settranscode(seed[0],strBankLog,strlen(strBankLog),strBCDtime,strlen(strBCDtime));
//
//        //   hexstring_to_hexarray(strPartnerData,strlen(strPartnerData),output);//get card type
//        /*2020-10-26,bugfix transaction record with jaklingko timestamp 00000000*/
//        //memset(empty,0x00,sizeof(empty));
//        //if(memcmp(&output[17],)
//        /*finish*/
//        //2021-08-02
//        DebugInteger("len_partner",len_partner);
//        if(len_partner!=128)
//        {
////            memset(output,0x00,64);
//            output[_OFFSET_TRANSTYPE_]=TJ_TYPE_NONBRT;
//            tapcode = atoi(strTapcode);
//            *trxtap=tapcode;
//            if(tapcode==_TAP_IN_)
//                output[_OFFSET_TAPCODE]=_TAP_IN_;
//            if(tapcode==_TAP_OUT_)
//                output[_OFFSET_TAPCODE]=_TAP_OUT_;
//            memcpy(&output[_OFFSET_TID_+8-sizeof(devinfo.TID)],devinfo.TID,sizeof(devinfo.TID));
//            memset(&output[_OFFSET_IDARMADA_],0x30,9);
//            memcpy(&output[_OFFSET_IDARMADA_+9-devinfo.lenArmada],deduct.Armada,devinfo.lenArmada);
//            memset(strPartnerData,0x00,sizeof(strPartnerData));
//            hexarray_to_hexstring(output,64,strPartnerData,NULL);
//            DebugStringLen("strPartnerData",strPartnerData,strlen(strPartnerData));
//        }
//        else
//        {
//            hexstring_to_hexarray(strPartnerData,strlen(strPartnerData),output);
//        }
//
//        tj_jaklingkoinfo(seed[0],output,strInterop,strlen(strInterop));
//        tj_setinterop(strInterop,strlen(strInterop),deductamount);//2021moved
//        memset(output,0x00,sizeof(output));
//        memset(seed,0x00,sizeof(seed));
//        tj_set_gatein_on(strBCDtime,strlen(strBCDtime));//2020-01-17,vivo
//        tj_set_gateout_on(strBCDtime,strlen(strBCDtime));//2020-01-17,vivo
//#endif dibawah ini lanjut


        offset=0;

        System.arraycopy(strCardType, 0, output, offset, strCardType.length);
        offset+=strCardType.length;
        DebugHex("output strCardType",output,offset);

        System.arraycopy(devInfo.MID, 0, output, offset, 8);
        offset+=8;
        DebugHex("output devInfo.MID", output,offset);

        System.arraycopy(devInfo.TID, 0, output, offset, 4);
        offset+=4;
        DebugHex("output devInfo.TID", output, offset);

        System.arraycopy(strBCDtime, 0, output, offset, strBCDtime.length);
        offset+=strBCDtime.length;
        DebugHex("output strBCDtime", output, offset);

        System.arraycopy(strCardNo, 0, output, offset, strCardNo.length);
        offset+=strCardNo.length;
        DebugHex("output strCardNo", output, offset);

        System.arraycopy(strDeduct, 0, output, offset, strDeduct.length);
        offset+=strDeduct.length;
        DebugHex("output strDeduct", output, offset);

        System.arraycopy(strBalance, 0, output, offset, strBalance.length);
        offset+=strBalance.length;
        DebugHex("output strBalance", output, offset);

        DebugString("trxid", ""+trxid);
        System.arraycopy(ByteUtils.int2bytes(trxid), 0, output, offset, 4);
        offset+=4;
        DebugHex("output trxid", output, offset);

        System.arraycopy(output, 0, oprlog, 0, offset);
        oprloglen=offset;
        DebugHex("Operator Log, oprlog",oprlog,oprloglen);
        DebugPrintString("Start Compose STI log");
        DebugPrintString("Generate random seed");
        for(i=0;i<seed.length;i++)
        {
            double rand = Math.random()*10;
            DebugString("random", ""+rand);
            seed[i]= (byte) rand;
        }
        DebugHex("seed",seed,seed.length);

        stiloglen=0;
        System.arraycopy(seed, 0, stilog, stiloglen, seed.length);
        stiloglen+=seed.length;
        DebugHex("stilog seed", stilog,stiloglen);

        System.arraycopy(output, 0, stilog, stiloglen, offset);
        stiloglen+=offset;
        DebugHex("stilog output",stilog,stiloglen);

        DebugStringHex("temp", temp);
        Log.d("strUID", ""+Hex.bytesToHexString(strUID)+"  "+strUID+"  "+Hex.bytesToASCIIString(strUID));
        System.arraycopy(strUID, 0, temp, 0, strUID.length);
        i=strUID.length;
        DebugStringHex("temp", temp);
        DebugString("i", ""+i);

        System.arraycopy(temp, 0, uid, 8-i, i);
        DebugStringHex("uid",uid);
        System.arraycopy(uid, 0, stilog, stiloglen, 8);
        stiloglen+=8;
        DebugHex("stilog uid", stilog,stiloglen);

        DebugHex("strPartnerData",strPartnerData,strPartnerData.length);
        System.arraycopy(strPartnerData, 0, stilog, stiloglen, strPartnerData.length);

        stiloglen+=strPartnerData.length;
        DebugHex("stilog strPartnerData", stilog,stiloglen);

        System.arraycopy(strExp, 0, stilog, stiloglen, strExp.length);
        stiloglen+=strExp.length;
        DebugHex("stilog strExp", stilog,stiloglen);

        byte[] bankLogLen = ByteUtils.int2bytes(strBankLog.length);
        DebugHex("bankLogLen", bankLogLen,bankLogLen.length);
        System.arraycopy(bankLogLen, 3, stilog, stiloglen, 1);
        stiloglen+=1;
        System.arraycopy(strBankLog, 0, stilog, stiloglen, strBankLog.length);
        stiloglen+=strBankLog.length;
        DebugHex("stilog strBankLog", stilog,stiloglen);

        offset=strBankLog.length;
        DebugString("banklog len",""+offset);

        stilog[stiloglen++]=0;
        stilog[stiloglen++]=0;
        DebugHex("stilog", stilog,stiloglen);

        i=Hex.ARC(Hex.subByte(stilog, 0, stiloglen), 0, stiloglen);
        DebugString("i", ""+i);

        temp=ByteUtils.int2dword(i, new byte[4]);
        DebugStringHex("temp",temp);

        System.arraycopy(temp, 2, stilog, stiloglen, 2);

        stiloglen+=2;
        DebugHex("stilog temp", stilog,stiloglen);

        i=stiloglen%8;
        if(i!=0)
            stiloglen+=(8-i);
        Log.d("DATA TO ENCRYPT",""+Hex.bytesToHexString(stilog)+" "+stiloglen);

        byte[] iv = new byte[8];
        Arrays.fill(iv,(byte)0x00);
        String key="42631544773B473B91FE2D45790985D2";
        des.LoadKey(Hex.hexStringToByteArray("42631544773B473B91FE2D45790985D242631544773B473B"));
//        des.LoadKey(Hex.hexStringToByteArray("256AFC459E6177AE3F36CCBA02F16C4F256AFC459E6177AE"));

        des.InitEncryption(iv);
        stilogencrypt=des.doEncrypt(Hex.subByte(stilog, 0, stiloglen));
        stilogencryptlen = stiloglen;
        DebugStringHex("stilogencrypt", stilogencrypt);

        System.arraycopy(oprlog, 0, outLog, 0,oprloglen);
        DebugStringHex("outlog", outLog);
        outLogLen+=oprloglen;
        System.arraycopy(stilogencrypt, 0, outLog, oprloglen, stilogencryptlen);
        outLogLen+=stilogencryptlen;
        DebugStringHex("outlog", outLog);

        offset=0;

        temp=new byte[768];
        temp[offset++]=0x02;
        offset+=2;
        DebugHex("temp", temp,offset);
        DebugStringHex("jaklingko_uploadtrxdata",jaklingko_uploadtrxdata);
        System.arraycopy(jaklingko_uploadtrxdata, 0, temp, offset, jaklingko_uploadtrxdata.length);
        offset+=jaklingko_uploadtrxdata.length;
        DebugHex("temp", temp,offset);

        temp[offset++]=TAG_TOKEN;
        baTagLen=ByteUtils.int2dword(Deduct.token.length, new byte[4]);
        DebugHex("baTagLen", baTagLen,offset);

        System.arraycopy(baTagLen, 2, temp, offset, 2);
        offset+=2;
        DebugHex("temp", temp,offset);
        System.arraycopy(Deduct.token, 0, temp, offset, Deduct.token.length);

        offset+=Deduct.token.length;
        DebugHex("temp", temp,offset);

        Log.d("strlat", ""+Hex.bytesToHexString(strLat)+" "+Hex.bytesToASCIIString(strLat));
        gpstemp=asciiToBCD(strLat, strLat.length);
        DebugStringHex("gpstemp", gpstemp);

        temp=ComposeTLV(TAG_LAT,gpstemp,(strLat).length/2, temp);
        DebugHex("temp", temp,offset);

        gpstemp=asciiToBCD(strLng, strLat.length);
        DebugStringHex("gpstemp", gpstemp);

        temp=ComposeTLV(TAG_LNG,gpstemp,(strLng.length)/2,temp);
        DebugHex("temp", temp,offset);

        tapcode = Integer.parseInt(Hex.bytesToHexString(strTapcode));
        DebugString("tapcode", ""+tapcode);
        trxtap=tapcode;
        if(tapcode==_TAP_IN_)
            strTapcode[0]= 'I';
        if(tapcode==_TAP_OUT_)
            strTapcode[0]= 'O';

        temp=ComposeTLV(TAG_TRXTYPE,strTapcode,1,temp);
        DebugHex("temp", temp,offset);
        temp=ComposeTLV(TAG_LOG,outLog,outLogLen,temp);
        DebugHex("temp", temp,offset);
        temp=ComposeTLV(TAG_MID, devInfo.MID,8,temp);
        DebugHex("temp", temp,offset);
        temp=ComposeTLV(TAG_TID,devInfo.TID,4,temp);
        DebugHex("temp", temp,offset);

        bInterop=strInterop;
        temp=ComposeTLV(TAG_CARDINTEROP,bInterop,bInterop.length,temp);
        DebugHex("temp", temp,offset);

        tjtype=Integer.parseInt(TjData.card_type);
        DebugString("tjtype", ""+tjtype);

        bInterop[0]= (byte) tjtype;
        temp=ComposeTLV(TAG_TJCARDTYPE,bInterop,1,temp);
        DebugHex("temp", temp,offset);

        crc=ByteUtils.int2dword(offset-3, new byte[4]);
        System.arraycopy(crc, 2, temp, 1, 2);
        DebugStringHex("crc", crc);

        temp[offset++]=XORing(temp,offset-1,1);

        DebugHex("outlog", outLog,outLogLen);
        DebugHex("temp", temp,offset);
        System.arraycopy(temp, 0, outLog, 0, offset);
        DebugHex("outlog", outLog,offset);
        outLogLen=offset;
        trxNo=trxid;
        DebugString("outloglen", ""+outLogLen);

        return outLog;
    }

    int tj_uploaddata(/*tjprotocol* tjdata,*/byte[] out,int lenOut)
    {
        DebugPrintString("tj_uploaddata");
//        char sqlcmd[2000] = {0};
        int counter=0;
        int lenvalue;
        int lenvalue_2;
        int code=-1;
        String sqlcmd;
        if(TjData.loginSuccess==false)
        {

            tj_login();
            if(counter<3)
            {
                counter++;
            }
            else
            {
                return NOT_OK;
            }
        }

        sqlcmd=String.format("{"+
                "\"action\":\"bus_transaction\","+
                "\"token\":\"%s\","+
                "\"transaction\":"+
                "[{"+
                "\"armada_id\":\"%s\","+
                "\"plat\":\"%s\","+
                "\"no_body\":\"%s\","+
                "\"trip_id\":\"%s\","+
                "\"si_id\":\"%s\","+
                "\"gate_in\":%s,"+
                "\"deduct_gate_in\":%s,"+
                "\"shelter_code_gate_in\":\"%s\","+
                "\"terminal_code_gate_in\":\"%s\","+
                "\"gate_in_on\":\"%s\","+
                "\"shelter_code_gate_out\":\"%s\","+
                "\"terminal_code_gate_out\":\"%s\","+
                "\"gate_out_on\":\"%s\","+
                "\"card_no\":\"%s\","+
                "\"card_type\":\"%s\","+
                "\"interop\":\"%s\","+
                "\"min_balance\":\"%s\","+
                "\"balance_before\":\"%s\","+
                "\"normal_fare\":\"%s\","+
                "\"fare\":\"%s\","+
                "\"balance_after\":\"%s\","+
                "\"mid\":\"%s\","+
                "\"tid\":\"%s\","+
                "\"trans_code\":\"%s\","+
                "\"long\":\"%s\","+
                "\"lat\":\"%s\","+
                "\"transportation_type\":\"%s\","+
                "\"jaklingko_first_gate_in\":\"%s\","+
                "\"jaklingko_subsidy_accumulation\":\"%s\","+
                "\"journey_counter\":\"%s\","+
                "\"expiration_on\":%s,"+
                "\"status\":\"%s\","+
                "\"desc\":\"%s\","+
                "\"free_service\":%s"+
                "}]"+
                "}", TjData.token,TjData.armadaid,TjData.plat,TjData.no_body,TjData.trip_id,TjData.si_id,TjData.gate_in,TjData.deduct_gate_in,TjData.shelter_code_gate_in,TjData.terminal_code_gate_in
                ,TjData.gate_in_on,TjData.shelter_code_gate_out,TjData.terminal_code_gate_out,TjData.gate_out_on,Hex.bytesToHexString(TjData.card_no),TjData.card_type,TjData.interop,TjData.min_balance
                ,TjData.balance_before,TjData.normal_fare,TjData.fare,TjData.balance_after,TjData.mid,TjData.tid,TjData.trans_code,TjData.lng,TjData.lat,TjData.transportation_type,TjData.jaklingko_first_gate_in
                ,TjData.jaklingko_subsidy_accumulation,TjData.journey_counter,TjData.expiration_on,TjData.status,TjData.desc,TjData.gratis);

        lenOut=sqlcmd.length();
//        DebugInteger("lenOut",*lenOut);
        DebugString("sqlcmd",sqlcmd);
        lenOut=0;//2020-12-23,reset respon length
//        memset(out,0x00,1000);//2020-12-23,clear buffer content
//        tj_senddata(sqlcmd,strlen(sqlcmd),out,lenOut);

        out = socketThread.sendData(sqlcmd.getBytes());
        Log.d("responsetjsenddata", ""+Hex.bytesToASCIIString(out));
        DebugHex("out", out, out.length);
//        DebugStringLen("out",out,*lenOut);
//
//
        String value;
//
//
//        memset(value,0x00,sizeof(value));

        value=jsonGetValue(out, "code");
        lenvalue=value.length();
        DebugString("code value",value);
        if(lenvalue>0)
        {

//            code=atoi(value);
//            DebugInteger("code",code);
//            switch(code)
//            {
//                case 0:
//                    DebugPrintString("data upload successful");
//                    lenvalue_2=jsonGetValue(out, "message", value);
//                    DebugStringLen("message",value,lenvalue_2);
//                    return OK;
//                case 110:
//                case 203:
//                    DebugPrintString("duplicate found");
//                    lenvalue_2=jsonGetValue(out, "message", value);
//                    DebugStringLen("message",value,lenvalue_2);
//                    return OK;
//                default:
//                    DebugPrintString("data upload fail");
//                    lenvalue_2=jsonGetValue(out, "message", value);
//                    DebugStringLen("message",value,lenvalue_2);
//                    return NOT_OK;
//
//
//            }

        }
        else
        {
            DebugPrintString("No code response received");
        }

        return OK;

    }

    private void DebugPrintString(String str) {
        Log.d(TAG, str);
    }

    private void DebugString(String str1, String str2) {
        Log.d(str1, str2);
    }

    private void DebugStringHex(String str, byte[] bytes) {
        Log.d(str, ""+Hex.bytesToHexString(bytes));
    }

    private void DebugHex(String stroutput, byte[] output, int offset) {
        Log.d(stroutput, ""+Hex.bytesToHexString(Hex.subByte(output, 0, offset)));
        Log.d("len", ""+offset);
    }

    long Calc_CRC_C_ARC(byte[] bufferLocal, int lenLocal)
    {
        int i,j;
        long crc;
        byte bufdat;
        long crc_seed = 0;
       // byte buffer = bufferLocal[0];

        crc = crc_seed;
//        while (lenLocal--)
        for(j=0;j<lenLocal;j++)
        {
            bufdat = bufferLocal[j];
//            Log.d("bufdat", j+" "+Hex.byteToHexString(bufdat));
            crc = crc ^ bufdat;
            for (i = 0;i < 8; i++)
            {
                if ((crc & 0x0001) !=0)
                {
// Expected poly should be bits reflected so 0xA001 used instead of 0x8005;
                    crc = (crc >> 1) ^ 0xA001;
                }
                else
                {
                    crc = crc >> 1;
                }
            }
        }
// Swapping final CRC nibbles
        //crc = (crc >> 8) | (crc << 8);
        // printf("CRC ARC: Seed: %.4x; Val: %.4x\r\n",crc_seed, crc);
        //memcpy(crcOut,&crc,2);
        Log.d("crc", ""+crc);
        return crc;

    }

    private String sqlfx_readout_select(String colName, String query) {
        Log.d("colname", ""+colName);
        ArrayList<String> arrayList = trxHistoryHelper.getDataByColumn(colName, query);
//        byte[] y=new byte[arrayList.size()];
        String y = null;
        for(int i=0;i<arrayList.size();i++){
            Log.d("geti", ""+arrayList.get(i));
            y=arrayList.get(i);
        }
        return y;
    }

//    private byte[] sqlfx_readout_select(String colName, String query) {
//        Log.d("colname", ""+colName);
//        ArrayList<String> arrayList = trxHistoryHelper.getDataByColumn(colName, query);
//        byte[] y=new byte[arrayList.size()];
////        String y = null;
//        for(int i=0;i<arrayList.size();i++){
//            Log.d("geti", ""+arrayList.get(i));
//            if(arrayList.get(i).length()==1){
//                y[i]=Hex.hexStringToByte(arrayList.get(i));
//                Log.d("y[i]", ""+Hex.byteToHexString(y[i]));
//            }else {
//                y=arrayList.get(i).getBytes();
////                y=Hex.hexStringToByteArray(arrayList.get(i));
//                Log.d("y", ""+Hex.bytesToHexString(y));
//                byte[] z = asciiToBCD(y, y.length);
//                Log.d("z", ""+Hex.bytesToHexString(z));
////                if(arrayList.get(i).equals("-1")){
////                    byte[] z = IntToBCD(1);
////                    Log.d("z", ""+Hex.bytesToHexString(z));
////                }
//            }
//            Log.d("ycolumn", ""+i+" "+y[i]);
////            if(i==lastSelectColCount){
////
////                lastSelectColCount++;
////            }
//
//        }
//        return y;
//    }

    private static byte[] asciiToBCD(byte[] ascii, int asc_len) {
        byte[] bcd = new byte[asc_len ];
        int j = 0;
        for (int i = 0; i < (asc_len + 1) / 2; i++) {
            bcd[i] = asc_to_bcd(ascii[j++]);
            bcd[i] = (byte) (((j >= asc_len) ? 0x00 : asc_to_bcd(ascii[j++])) + (bcd[i] << 4));
        }/*from  w  w w .j  av  a  2 s .  com*/
        return bcd;
    }

    private static byte asc_to_bcd(byte asc) {
        byte bcd;

        if ((asc >= '0') && (asc <= '9'))
            bcd = (byte) (asc - '0');
        else if ((asc >= 'A') && (asc <= 'F'))
            bcd = (byte) (asc - 'A' + 10);
        else if ((asc >= 'a') && (asc <= 'f'))
            bcd = (byte) (asc - 'a' + 10);
        else
            bcd = (byte) (asc - 48);
        return bcd;
    }

    static byte[] IntToBCD(int input) {
        byte[] bcd = new byte[] {
                (byte)(input>> 8),
                (byte)(input& 0x00FF)
        };
        return bcd;
    }
//
//    private String sqlfx_read_select(String colName){
//        int i;
//        for(i=0; i<lastSelectColCount; i++){
//
//        }
//    }
////
//    char* sqlfx_read_select(char* colName)
//    {
//        int i;
//
//        for(i=0; i<lastSelectColCount; i++)
//        {
//            if(strcmp(colName, lastSelectColNames[i]) == 0)
//                return lastSelectColValues[i];
//        }
//        thisPrintf("Unable to locate colName=%s\n", colName);
//        return 0;
//    }


    void Task_DataUpload()
    {
//        DebugPrintString("Task_DataUpload");
        int iStart;
        int iFinish;
        int counter=0;
        int i;
        byte[] uploadbuffer=new byte[1000];
        int uploadlen = 0;
        int trxid = 0;
        byte[] temp=new byte[1000];
        byte[] time=new byte[7];
        int tempLen;
        int iRet;
        int tapcode = 0;
        int iResp;
        byte[] synctime=new byte[7];
        byte[] timebcd=new byte[20+1];
        int uploadcnt=0;
        int touploadcnt=0;
        int iRespLen = 0;

//        iStart=gettickcount();
//        iFinish = iStart;

//        while(1)
//        {
//            if((iFinish-iStart) < deduct.lUploadWindow*60)
//            //if((iFinish-iStart) < deduct.lUploadWindow)
//            {
//                sleepms(1000);
//                iFinish=gettickcount();
//                continue;
//            }
//            if(deduct.downloadupdate==true)
//            {
//                iFinish=gettickcount();
//                iStart=iFinish;
//                continue;
//            }
//            if(deduct.reboot)//2019-12-17,vivo, if reader request to reboot, cancel all data upload
//            {
//                iFinish=gettickcount();
//                iStart=iFinish;
//                continue;
//            }
//            if(deduct.cardexist)
//            {
//                DebugPrintString("Task_DataUpload : Card detected, cancel data upload");
//                iFinish=gettickcount();
//                iStart=iFinish;
//                SetDBIdle();
//                continue;
//            }
//            if(qrcode.available==true)
//            {
//                DebugPrintString("Task_DataUpload : qr available, cancel data upload");
//                iFinish=gettickcount();
//                iStart=iFinish;
//                continue;
//            }
//
//            if(devinfo.dbBusy)
//            {
//                DebugPrintString("Database is busy, cancel data upload and reset upload timer");
//                iFinish=gettickcount();
//                iStart=iFinish;
//                continue;
//            }
////        if (devinfo.online==false)
////        {
////                DebugPrintString("Modem off, cancel data upload");
////                iFinish=gettickcount();
////                iStart=iFinish;
////                continue;
////        }
//            DebugBoolean("deduct.fLoginSuccess",deduct.fLoginSuccess);
//            if(deduct.fLoginSuccess==false)
//            {
//                DebugPrintString("Device operated in offline mode, do login 1st before upload data");
//                counter=0;
//                DebugInteger("counter",counter);
//                trialConnect=1;
//                while(counter<MAX_TRIAL_LOGIN)
//                {
//                    iRet=tcp_to_host();
//                    if(iRet!=OK)
//                    {
//                        DebugPrintString("FAIL :  Logon");
//                        counter++;
//                        if(counter==MAX_TRIAL_LOGIN)
//                        {
//                            DebugPrintString("Reset upload timer");
//                            iFinish=gettickcount();
//                            iStart=iFinish;
//                        }
//                    }
//                    else
//                    {
//                        counter=MAX_TRIAL_LOGIN+1;
//                        DebugPrintString("SUCCESS :  Logon,Device now operating in online mode");
//                        deduct.fLoginSuccess=true;
//                        //display_dataon(processbuf,0,processbuf,&dataLen);
//                        scpi_icon(DATAON);
//                    }
//                }
//                if(counter==MAX_TRIAL_LOGIN)
//                {
//                    continue;
//                }
//            }
//            if(deduct.deductAction==REPURCHASE||deduct.deductAction==REWRITEPARTNERDATA)//2019-12-20,vivo,check whether reader in repurchase mode
//            {
//                DebugPrintString("application is in correction mode, cancel data upload");
//                DebugPrintString("Reset upload timer");
//                iFinish=gettickcount();
//                iStart=iFinish;
//                DebugInteger("iFinish",iFinish);
//                DebugInteger("iStart",iStart);
//                DebugPrintString("Reset upload counter");
//                counter=0;
//                DebugInteger("counter",counter);
//                deduct.uploading=false;
//            }
//            //else
//            if(deduct.deductAction==NORMAL)//2019-12-20,vivo
//            {
//
//                DebugPrintString("application is in normal mode, execute transaction counter update and data upload");
//                DebugPrintString("Insert record from backup files");
//
//                InsertBackupDatatoDB_duplicatecheck();
//
//                DebugPrintString("Start data upload process");
//                DebugHex("trxtime.bStart",trxtime.bStart,sizeof(trxtime.bStart));
//                hex_yymmddhhmmss_to_bcd_ddmmyyhhmmss(trxtime.bStart, time);
//
//
//                DebugHex("time",time,sizeof(time));
//                deduct.uploading=true;
//
//                DebugInteger("deduct.maxrecordupload",deduct.maxrecordupload);
//
//                iStart=gettickcount();
//                DebugInteger("iStart",iStart);
//                while(counter<deduct.maxrecordupload)
//                {
//                    DebugInteger("counter",counter);
//                    if(deduct.reboot)//2019-12-17,vivo, if reader request to reboot, cancel all data upload
//                    {
//                        DebugPrintString("Reboot requested, cancel data upload");
//                        break;
//                    }
//                    if(deduct.cardexist)
//                    {
//                        DebugPrintString("Card detected, cancel data upload");
//                        counter=deduct.maxrecordupload;
//                        DebugInteger("iRet",iRet);
//                        continue;
//                    }
//
                    outLogLen=uploadlen;
                    uploadbuffer=payment_selecttrxrecord_TLV(tapcode,uploadbuffer,trxid);
                    i=outLogLen;

                    Log.d("ip",""+ip+" "+port);
                    socketThread.connect(ip, port);

                    Log.d("outloglen", ""+outLogLen);
                    Log.d("uploadbuffer", ""+Hex.bytesToHexString(uploadbuffer));
                    Log.d("uploadbuffer", ""+Hex.bytesToHexString(Hex.subByte(uploadbuffer, 0, outLogLen)));
                    byte[] resp = socketThread.sendData(Hex.subByte(uploadbuffer, 0, outLogLen));

//                    uploadbuffer=payment_selecttrxrecord_TLV(tapcode,uploadbuffer,uploadlen,trxid);
//                    if(i!=OK)
//                    {
//                        DebugPrintString("Fail to read transaction record, stop sending data");
//                        counter=deduct.maxrecordupload;
//
//
//                        DebugInteger("iRet",iRet);
//                        continue;
//                    }
//                    scpi_icon(SYNCON);
//#ifdef _TOB_
//                    DebugPrintString("Send record to tj server");
//                    i=tj_uploaddata(&tjdata,temp,&tempLen);
//                    if(i==OK)
//                    {
//
//                        tempLen=0;
//                        //i=pcd_SendData(uploadbuffer,uploadlen,dbbuffer.temp,&tempLen);
//                        i=pcd_SendData(uploadbuffer,uploadlen,temp,&tempLen);
//
//
//                        //CodeDesc("pcd_SendData i",i);
//                        memset(synctime,0x00,sizeof(synctime));
//                        hex_yymmddhhmmss_to_bcd_ddmmyyyyhhmmss(trxtime.bStart,synctime);
//                        DebugHex("hex_yymmddhhmmss_to_bcd_ddmmyyyyhhmmss synctime",synctime,sizeof(synctime));
//                        hexarray_to_hexstring(synctime,7,timebcd,NULL);
//                        if(i==OK)
//                        {
//
//                            sprintf(temp, "UPDATE TRXHISTORY SET SYNC='1',SYNCTIME='%s' WHERE id='%d'",timebcd, trxid);
//                            //int iResp = sqlfx_exec_update(paymentDb, temp);
//                            iResp = sqlfx_exec_update(paymentDb, temp);
//                            DebugInteger("sqlfx_exec_update iResp",iResp);
//                            if(iResp<=0)
//                            {
//                                DebugPrintString("DB: Failed to update synced txnRecord");
//                                //if fail to update trx record, cancel data upload
//                                counter=deduct.maxrecordupload;
//
//                                //continue;
//                                //return ;
//                            }
//                            else
//                            {
//                                DebugPrintString("DB: Increase uploaded counter");
//                                CalculateSyncRecord(trxtime.bStart,7,&uploadcnt);
//                                CalculateUnsyncRecord(&touploadcnt);
//                                draw_trxCount(true,uploadcnt,touploadcnt,processbuf,&iRespLen);
//                            }
//                            counter++;
//                        }
//                        else
//                        {
//                            memset(synctime,0x00,sizeof(synctime));
//                            hex_yymmddhhmmss_to_bcd_ddmmyyyyhhmmss(trxtime.bStart,synctime);
//                            DebugHex("hex_yymmddhhmmss_to_bcd_ddmmyyyyhhmmss synctime",synctime,sizeof(synctime));
//                            hexarray_to_hexstring(synctime,7,timebcd,NULL);
//                            DebugPrintString("Upload record fail, set record sync to 2, means record fail to STI server");
//                            //sprintf(temp, "UPDATE TRXHISTORY SET SYNC='2' WHERE id='%d'", trxid);
//                            sprintf(temp, "UPDATE TRXHISTORY SET SYNC='2' WHERE id='%d'", trxid);
//
//                            //int iResp = sqlfx_exec_update(paymentDb, temp);
//                            iResp = sqlfx_exec_update(paymentDb, temp);
//                            DebugInteger("sqlfx_exec_update iResp",iResp);
//                            if(iResp<=0)
//                            {
//                                DebugPrintString("DB: Failed to update synced txnRecord");
//                                //if fail to update trx record, cancel data upload
//                                counter=deduct.maxrecordupload;
//
//
//                                //return ;
//                            }
//                            else
//                            {
//                                DebugPrintString("DB: Record sync flag updated");
//                            }
//                            counter++;
//                        }
//
//                    }
//                    else
//                    {
//                        //DebugPrintString("Fail to upload data to TJ server,stop sending data");
//                        //DebugPrintString("Upload record fail, set record sync to 2, means record fail to upload");
//                        //sprintf(temp, "UPDATE TRXHISTORY SET SYNC='2' WHERE id='%d'", trxid);
//                        memset(synctime,0x00,sizeof(synctime));
//                        hex_yymmddhhmmss_to_bcd_ddmmyyyyhhmmss(trxtime.bStart,synctime);
//                        DebugHex("hex_yymmddhhmmss_to_bcd_ddmmyyyyhhmmss synctime",synctime,sizeof(synctime));
//                        hexarray_to_hexstring(synctime,7,timebcd,NULL);
//                        DebugPrintString("Upload record fail to tj server, set record sync to corresponding error code");
//                        sprintf(temp, "UPDATE TRXHISTORY SET SYNC='%d',SYNCTIME='%s' WHERE id='%d'",i,timebcd, trxid);
//                        // int iResp = sqlfx_exec_update(paymentDb, temp);
//                        iResp = sqlfx_exec_update(paymentDb, temp);
//                        DebugInteger("sqlfx_exec_update iResp",iResp);
//                        if(iResp<=0)
//                        {
//                            DebugPrintString("DB: Failed to update synced txnRecord");
//                            //if fail to update trx record, cancel data upload
//                            counter=deduct.maxrecordupload;
//
//                            //continue;
//                            //return ;
//                        }
//                        else
//                        {
//                            DebugPrintString("DB: Record sync flag updated");
//                        }
//                        counter++;
//                    }
//#endif
//
//                }
//                iFinish=gettickcount();
//                DebugInteger("iFinish",iFinish);
//                DebugInteger("Task_DataUpload upload duration (ms)",iFinish-iStart);
//                DebugPrintString("Reset upload timer");
//                iFinish=gettickcount();
//                iStart=iFinish;
//
//                DebugPrintString("Reset upload counter");
//                counter=0;
//                DebugInteger("counter",counter);
//            /*
//             * //2020-11-19,vivo, deactivated because cause blocking if record is rejected by TJ server 
//            iRet=UnsyncErrorTransactionData();//2020-06-05,vivo, reset unsuccessful data upload for resend
//            */
//                DebugPrintString("Try to insert record from backup files again");
//
//                InsertBackupDatatoDB_duplicatecheck();
//                deduct.uploading=false;
//                scpi_icon(SYNCOFF);
//
//
//            }
//
//        }

    }

    int tj_setfares(byte[] normalfare,byte[] fare,byte[] minbalance,byte[] balanceafter)
    {
        int len;
        int value;
        int balancebefore;
        value=ByteUtils.bytesToInt(normalfare, 0);
        Log.d("value",""+value);
        TjData.normal_fare=String.format("%s",value);
        Log.d("tjdata.normal_fare",TjData.normal_fare);
//        sprintf(tjdata.normal_fare,"%i",value);
//        DebugString("tjdata.normal_fare",tjdata.normal_fare);
        value=ByteUtils.bytesToInt(balanceafter, 0);
        Log.d("value",""+value);
        TjData.balance_after=String.format("%s",value);
        Log.d("tjdata.balance_after",TjData.balance_after);
//        value=dword2int(balanceafter);
//        DebugInteger("value",value);
//        sprintf(tjdata.balance_after,"%i",value);
//        DebugString("tjdata.balance_after",tjdata.balance_after);
        balancebefore=value;

        value=ByteUtils.bytesToInt(fare, 0);
        Log.d("value",""+value);
        TjData.fare=String.format("%s",value);
//        value=dword2int(fare);
//        DebugInteger("value",value);
//        sprintf(tjdata.fare,"%i",value);
        balancebefore+=value;
        TjData.balance_before=String.format("%s",balancebefore);
        Log.d("tjdata.balance_before",TjData.balance_before);
//        sprintf(tjdata.balance_before,"%i",balancebefore);
//        DebugString("tjdata.balance_before",tjdata.balance_before);
        value=ByteUtils.bytesToInt(minbalance, 0);
        Log.d("value",""+value);
        TjData.min_balance=String.format("%s",value);
        Log.d("tjdata.min_balance",""+TjData.min_balance);
//        value=dword2int(minbalance);
//        DebugInteger("value",value);
//        sprintf(tjdata.min_balance,"%i",value);
//        DebugString("tjdata.min_balance",tjdata.min_balance);

        return 0;
    }

    int tj_latlng(byte[] lat,int latlen, byte[] lng,int lnglen)
    {
//        DebugPrintString("tj_latlng");
        Log.d(TAG, "tj_latlng");
        int len;
        byte[] temp=new byte[20];
        temp=ByteUtils.hexStringToByteArray(lat.toString());
        //len=hexstring_to_hexarray(lat,latlen,temp);
        if(temp.length>0)
        {
            TjData.lat=String.format("%.*s",temp.length,temp);
//            sprintf(tjdata.lat,"%.*s",len,temp);
        }
        else
        {
            TjData.lat="0";
            //sprintf(tjdata.lat,"0");
        }
       // DebugString("tjdata.lat",tjdata.lat);
        temp=ByteUtils.hexStringToByteArray(lng.toString());
        if(temp.length>0)
        {
            TjData.lng=String.format("%.*s",temp.length,temp);
        }
        else
        {
            TjData.lng="0";
        }
        //DebugString("tjdata.lng",tjdata.lng);
        //memcpy(tjdata.lat,lat,latlen);
        //memcpy(tjdata.lng,lng,lnglen);

        return 0;
    }

    int tj_setcardno(byte[] data,int lendata)
    {
        int len;
        System.arraycopy(data, 0, TjData.card_no, 0, lendata);
//        memcpy(tjdata.card_no,data,lendata);
        len=lendata;
//        DebugStringLen("tjdata.card_no",tjdata.card_no,len);
        return 0;
    }

    byte[] pcd_cmd_login(byte[] username,byte[] password)
    {
        byte[] loginData=new byte[1024];
        int lenData = 0;
        System.arraycopy(new byte[]{0x02}, 0, loginData, 0, 1);
        lenData+=1;
        byte[] length=new byte[2];
        System.arraycopy(length, 0, loginData, lenData, length.length);
        lenData+=length.length;
        byte[] id=new byte[]{(byte) 0xab, 0x01};
        System.arraycopy(id, 0, loginData, lenData, id.length);
        lenData+=id.length;
        byte[] latitude=new byte[]{0x01, 0x00, 0x00};
        System.arraycopy(latitude, 0, loginData, lenData, latitude.length);
        lenData+=latitude.length;
        byte[] longitude=new byte[]{0x02, 0x00, 0x00};
        System.arraycopy(longitude, 0, loginData, lenData, longitude.length);
        lenData+=longitude.length;
        loginData[lenData++]=0x03;
        lenData++;
        loginData[lenData++]=16;
        byte[] sn = Hex.hexStringToByteArray(getSerialNumber());
        System.arraycopy(Hex.str2HexStr(Hex.bytesToHexString(new byte[8])), 0, loginData, lenData, 8);
        lenData+=8;
        System.arraycopy(Hex.str2HexStr(Hex.bytesToHexString(sn)), 0, loginData, lenData, sn.length*2);
        lenData+=sn.length*2;
        byte[] appVersion=new byte[]{0x04, 0x00, 0x03, 0x00, 0x00, 0x01};
        System.arraycopy(appVersion, 0, loginData, lenData, appVersion.length);
        Log.d("logindata1", ""+Hex.bytesToHexString(loginData));
        lenData+=appVersion.length;
        length = Hex.IntegerToByteArray(lenData-3);
        Log.d("length", ""+Hex.bytesToHexString(length));
        System.arraycopy(length,2,loginData,1,2);
        Log.d("logindata2", ""+Hex.bytesToHexString(loginData));
        loginData[lenData++]=calculateLRC(loginData, 1, lenData - 1);
        Log.d("Logindata", ""+Hex.bytesToHexString(loginData));
        return Hex.subByte(loginData, 0, lenData);

    }

    private static String getSerialNumber(){
        String serialNumber;
        try{
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get= c.getMethod("get", String.class);
            serialNumber=(String) get.invoke(c, "gsm.sn1");
            if(serialNumber.equals(""))
                serialNumber=(String) get.invoke(c, "ril.serialnumber");
            if(serialNumber.equals(""))
                serialNumber=(String) get.invoke(c, "ro.serialno");
            if(serialNumber.equals(""))
                serialNumber=(String) get.invoke(c, "sys.serialnumber");
            if(serialNumber.equals(""))
                serialNumber= Build.SERIAL;
            if(serialNumber.equals(Build.UNKNOWN))
                serialNumber=null;
        } catch (Exception e) {
            e.printStackTrace();
            serialNumber=null;
        }
        return serialNumber;
    }
}
